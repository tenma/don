<?php

namespace LQDN\Tests\Functional\Finder;

use LQDN\Tests\Functional\FunctionalTest;

class AdminFinderTest extends FunctionalTest
{
    public function testFindAll()
    {
        $admins = $this->container['admin_finder']->findAll();
        $this->assertCount(1, $admins);

        // Check the first address
        $expectedAdmin = [
            'id' => 1,
            'user_id' => 'admin',
        ];
        $this->assertEquals($expectedAdmin, $admins[0]);
    }
}
