<?php

namespace LQDN\Tests\Functional\Finder;

use LQDN\Tests\Functional\FunctionalTest;

class AddressFinderTest extends FunctionalTest
{
    public function testFindByUserId()
    {
        $addresse = $this->container['address_finder']->findByUserId(1);
        $this->assertCount(9, $addresse);

        // Check the address have an ID of 1
        $expectedAddress = [
            'id' => '1',
            'nom' => 'Main',
            'adresse' => '1 rue Ménars',
            'adresse2' => null,
            'codepostal' => '75001',
            'ville' => 'Paris',
            'etat' => null,
            'pays' => 'France',
            'user_id' => '1',
        ];
        $this->assertEquals($expectedAddress, $addresse);
    }

    public function testFindByCounterpartId()
    {
        $addresse = $this->container['address_finder']->findByCounterpartId(1);
        $this->assertCount(9, $addresse);

        // Check the address have an ID of 1
        $expectedAddress = [
            'id' => '1',
            'nom' => 'Main',
            'adresse' => '1 rue Ménars',
            'adresse2' => null,
            'codepostal' => '75001',
            'ville' => 'Paris',
            'etat' => null,
            'pays' => 'France',
            'user_id' => '1',
        ];
        $this->assertEquals($expectedAddress, $addresse);
     }
}
