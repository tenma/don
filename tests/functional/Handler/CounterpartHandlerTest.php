<?php

namespace LQDN\Tests\Functional\Handler;

use LQDN\Command\CounterpartCreateCommand;
use LQDN\Command\CounterpartDeleteCommand;
use LQDN\Command\CounterpartChangeStateCommand;
use LQDN\Command\AdminUpdateParentCommand;
use LQDN\Handler\CounterpartHandler;
use LQDN\Tests\Functional\FunctionalTest;

class CounterpartHandlerTest extends FunctionalTest
{
    public function testAdminUpdateParentCommand()
    {
        $this->container['command_handler']->handle(new AdminUpdateParentCommand());
        $target = $this->container['counterpart_finder']->findById(4);
        $this->assertSame((int) $target['parent'], 4);
    }

    public function testCounterpartDelete()
    {
        $this->assertTrue($this->counterpartExists(1));

        $this->container['command_handler']->handle(new CounterpartDeleteCommand(1));
    }

    public function testCounterpartCreate()
    {
        $this->assertFalse($this->counterpartExists(5));

        $this->container['command_handler']->handle(new CounterpartCreateCommand(1, 1, 'pishirt', 4, 1, date("Y-m-d H:i:s"), '', 1));
    }

    public function testCounterpartChangeState()
    {
        $this->assertTrue($this->counterpartExists(1));

        $this->container['command_handler']->handle(new CounterpartChangeStateCommand(1, 2));
    }

    /**
     * Check if a counterpart exists in BDD
     *
     * @param int $id
     *
     * @return bool
     */
    private function counterpartExists($id)
    {
        return (bool) $this->container['db']->fetchColumn("SELECT 1 FROM contreparties WHERE id = $id");
    }
}
