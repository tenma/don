<?php

namespace LQDN\Tests\Functional;

use LQDN\Container;

abstract class FunctionalTest extends \PHPUnit_Framework_TestCase
{
    protected static $shouldTerminateTransaction = false;

    protected $container;

    protected function setUp()
    {
        $this->container = new Container();
        $this->container->extend('db', function ($db, $c) {
            $db->query('START TRANSACTION;');

            FunctionalTest::$shouldTerminateTransaction = true;

            return $db;
        });
    }

    protected function tearDown()
    {
        if (!self::$shouldTerminateTransaction) {
            return;
        }

        $this->container['db']->query('ROLLBACK;');
    }
}
