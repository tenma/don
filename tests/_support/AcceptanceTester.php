<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    /**
     * Authenticate a user.
     *
     * @param string $email
     * @param string $password
     */
    public function login($email, $password)
    {
        $I = $this;
        $I->amOnPage('/');
        $I->fillField('email', $email);
        $I->fillField('password', $password);
        $I->click('submit');
        $I->see('Se déconnecter', '.navbar');
    }

    /**
     * @Given I am on page :arg1
     */
     public function iAmOnPage($arg1)
     {
         $this->amOnPage($arg1);
     }

    /**
     * @Then I should see :arg1
     */
     public function iShouldSee($arg1)
     {
         $this->see($arg1);
     }


}
