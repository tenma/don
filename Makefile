.PHONY: help doctor install reset-db translations
.DEFAULT_GOAL := help

ifeq ($(shell test -e app/env && echo -n yes),yes)
include app/env
export $(shell sed 's/=.*//' app/env)
endif

MYSQL_CONNECTION_STRING = -h$(SQL_HOST) -u$(SQL_USER)
ifdef SQL_PASSWORD
	MYSQL_CONNECTION_STRING += -p$(SQL_PASSWORD)
endif

help:
	@echo "\033[33mUsage:\033[0m"
	@echo "  make [command]"
	@echo ""
	@echo "\033[33mAvailable commands:\033[0m"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' Makefile | sort \
		| awk 'BEGIN {FS = ":.*?## "}; {printf "  \033[32m%s\033[0m___%s\n", $$1, $$2}' | column -ts___

doctor: ## Check that everything is installed to use this application
	@echo "\033[1m\033[36m==> Check required dependencies\033[0m\033[21m"
	@which composer >/dev/null 2>&1 && echo "\033[32mcomposer installed\033[0m" || echo "\033[31mcomposer not installed\033[0m"
	@which mysql >/dev/null 2>&1 && echo "\033[32mmysql installed\033[0m" || echo "\033[31mmysql not installed\033[0m"
	@echo "\033[1m\033[36m==> Check configuration\033[0m\033[21m"
	@test -s ./app/env && echo "\033[32mEnvironment config OK\033[0m" || echo "\033[31mYou need to copy app/env.sample to app/env in order to configure your application.\033[0m"
	@test -s ./app/config.php && echo "\033[32mConfiguration OK\033[0m" || echo "\033[31mYou need to copy app/config.php.sample to app/config.php in order to configure your application.\033[0m"
	@echo "\033[1m\033[36m==> Check optional dependencies\033[0m\033[21m"
	@which msgmerge >/dev/null 2>&1 && echo "\033[32mmsgmerge installed\033[0m" || echo "\033[31mmsgmerge not installed\033[0m"
	@which msgfmt >/dev/null 2>&1 && echo "\033[32mmsgfmt installed\033[0m" || echo "\033[31mmsgfmt not installed\033[0m"
	@which xgettext >/dev/null 2>&1 && echo "\033[32mxgettext installed\033[0m" || echo "\033[31mxgettext not installed\033[0m"
	@which pdftk >/dev/null 2>&1 && echo "\033[32mpdftk installed\033[0m" || echo "\033[31mpdftk not installed\033[0m"
	@test -s /usr/share/php7.0-xml/xml/dom.ini && echo "\033[032mphp-ext-dom installed\033[0m" || echo "\033[31mphp-ext-dom not installed (php-xml)\033[0m"
	@test -s /usr/share/php7.0-zip/zip/zip.ini && echo "\033[032mphp-zip installed\033[0m" || echo "\033[31mphp-zip not installed\033[0m"
	@test -s /usr/share/php7.0-mbstring/mbstring/mbstring.ini && echo "\033[032mphp-mbstring installed\033[0m" || echo "\033[31mphp-mbstring not installed\033[0m"
	@test -s /usr/share/php7.0-curl/curl/curl.ini && echo "\033[032mphp-curl installed\033[0m" || echo "\033[31mphp-curl not installed\033[0m"

install: ## Install the application
	@echo "\033[1m\033[36m==> Install Composer dependencies\033[0m\033[21m"
	@composer -n install
	@mkdir -p ./log
	@mkdir -p ./tmp

reset-db: ## Install or re-install the DB
	@echo "\033[1m\033[36m==> Drop database "$(SQL_DATABASE)" if it already exists\033[0m\033[21m"
	mysql $(MYSQL_CONNECTION_STRING) -e "DROP DATABASE IF EXISTS $(SQL_DATABASE)"
	@echo "\033[1m\033[36m==> Create database "$(SQL_DATABASE)"\033[0m\033[21m"
	@mysql $(MYSQL_CONNECTION_STRING) -e "CREATE DATABASE $(SQL_DATABASE)"
	@echo "\033[1m\033[36m==> Create schema\033[0m\033[21m"
	@php vendor/bin/phinx migrate -c app/Resources/phinx.yml
	@echo "\033[1m\033[36m==> Loading fixtures\033[0m\033[21m"
	@php vendor/bin/phinx seed:run -c app/Resources/phinx.yml -s AdminSeeder -s UserSeeder -s AddressSeeder -s CounterpartSeeder -s DonationSeeder

test: test-functional test-acceptance cs-lint ## Launch tests

test-acceptance: ## Launch functional tests.
	@$(MAKE) reset-db
	@./vendor/bin/codecept run acceptance

test-functional: ## Launch functional tests.
	@$(MAKE) reset-db
	@./vendor/bin/codecept run functional

coverage: ## Launch functional tests with coverage.
	@$(MAKE) reset-db
	@./vendor/bin/codecept run functional --coverage-html

server-start: server-stop ## Launch a local server
	@php -S 127.0.0.1:8000 -t ./www/ >> ./log/server.log &
	@echo "\033[32mServer running. (http://127.0.0.1:8000)\033[0m"

server-stop: ## Stop local server if running
	-@ps -aux | grep "[p]hp -S 127.0.0.1:8000" | grep -v grep | awk '{print $$2}' | xargs -r -n 1 kill
	@echo "\033[32mServer stopped. (http://127.0.0.1:8000)\033[0m"

cs-fix: ## Fix CS
	@vendor/bin/php-cs-fixer fix

cs-lint: ## Lint
	@vendor/bin/php-cs-fixer fix --dry-run

translations: locales/fr_FR/LC_MESSAGES/messages.mo locales/en_US/LC_MESSAGES/messages.mo ## Generate translations

messages.pot: app/*.php app/view/*.html app/view/*/*.html
	[ -r $@ ] || touch $@
	xgettext --package-name=LQDNCampaign --package-version=2016.1 --force-po -o $@ --keyword=__ --keyword=_  --from-code=UTF-8 $^

locales/%/LC_MESSAGES/messages.po: messages.pot
	msgmerge -v -U $@ $^

locales/fr_FR/LC_MESSAGES/messages.mo: locales/fr_FR/LC_MESSAGES/messages.po
	msgfmt $^ -o $@

locales/en_US/LC_MESSAGES/messages.mo: locales/en_US/LC_MESSAGES/messages.po
	msgfmt $^ -o $@
