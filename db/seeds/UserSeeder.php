<?php

use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = array(
            array(
                'id' => 1,
                'email' => 'alice@example.org',
                'hash' => '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',
                'total' => 5000,
                'cumul' => 2000,
                'pseudo' => 'Alice',
                'expiration' => null,
                'status' => 1,
                'commentaire' => 'RAS',
            ),
            array(
                'id' => 2,
                'email' => 'bob@example.org',
                'hash' => '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',
                'total' => 110,
                'cumul' => 530,
                'pseudo' => 'Bob',
                'expiration' => null,
                'status' => 1,
                'commentaire' => 'RAS',
            ),
        );

        $this->table('users')->insert($data)->save();
    }
}
