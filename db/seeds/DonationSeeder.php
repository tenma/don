<?php

use Phinx\Seed\AbstractSeed;

class DonationSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = array(
            array(
                'id' => 1,
                'status' => 1,
                'datec' => '2016-06-10 12:34',
                'somme' => 1000,
                'lang' => 'fr_FR',
                'cadeau' => 1,
                'abo' => 1,
                'taille' => 2,
                'public' => 0,
                'pdf' => 'pdf',
                'decimale' => 2,
                'datee' => '2016-06-10 12:34',
                'mailsent' => 1,
                'color' => 'blue',
                'pi_x' => 3,
                'pi_y' => 3,
                'hash' => 'hash',
                'taille_h' => 8,
                'fdnn_user' => 1234567890,
                'color_2' => 'red',
                'cumul' => 1000,
                'adresse_id' => 1,
                'user_id' => 1,
		        'identifier' => 'id1',
            ),
            array(
                'id' => 2,
                'status' => 1,
                'datec' => '2016-06-10 12:34',
                'somme' => 1000,
                'lang' => 'fr_FR',
                'cadeau' => 1,
                'abo' => 1,
                'taille' => 8,
                'public' => 0,
                'pdf' => 'pdf',
                'decimale' => 2,
                'datee' => '2016-06-10 12:34',
                'mailsent' => 1,
                'color' => 'blue',
                'pi_x' => 3,
                'pi_y' => 3,
                'hash' => 'hash',
                'taille_h' => 8,
                'fdnn_user' => 1234567890,
                'color_2' => 'red',
                'cumul' => 1000,
                'adresse_id' => 3,
                'user_id' => 2,
		        'identifier' => 'id2',
            ),
            array(
                'id' => 3,
                'status' => 100,
                'datec' => '2016-06-11 12:34',
                'somme' => 100,
                'lang' => 'fr_FR',
                'cadeau' => 1,
                'abo' => 1,
                'taille' => 8,
                'public' => 1,
                'pdf' => 'pdf',
                'decimale' => 2,
                'datee' => '2016-06-10 12:34',
                'mailsent' => 1,
                'color' => 'blue',
                'pi_x' => 3,
                'pi_y' => 3,
                'hash' => 'hash',
                'taille_h' => 8,
                'fdnn_user' => 1234567890,
                'color_2' => 'red',
                'cumul' => 1000,
                'adresse_id' => 1,
                'user_id' => 2,
        		'identifier' => 'id3',
            )
        );

        $this->table('dons')->insert($data)->save();
    }
}
