<?php


use Phinx\Migration\AbstractMigration;

class AddParentField extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('contreparties');
        if (!$table->hasColumn('parent')) {
            $table->addColumn('parent', 'integer');
            $table->save();
        }

        // Let's set some default value
        $builder = $this->getQueryBuilder();
        $stmt = $builder->update('contreparties', 'c')
            ->set('parent', 'id')
//            ->where('parent = 0')
            ->execute();
    }

    public function down()
    {
        $table = $this->table('contreparties');

        if ($table->hasColumn('parent')) {
            $table->removeColumn('parent')
                ->save();
        }
    }
}
