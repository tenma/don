<?php


use Phinx\Migration\AbstractMigration;

class UneSeuleAdresse extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        // Removing the alias <> user_id index
        $table = $this->table('adresses');
        $table->removeIndex(['alias', 'user_id'])
            ->save();

        // Deleting all addresses, to be sure we don't have doubles
        $this->execute('TRUNCATE TABLE adresses;');

        // Add a unique index on user_id
        $table->addForeignKey('user_id', 'users', 'id', ['delete' => 'CASCADE'])
            ->removeColumn('alias')
            ->save();

    }

    public function down() {
        $table = $this->table('adresses');
        $table->addColumn('alias', 'string')
            ->removeIndex(['user_id'])
            ->save();
        $table->addIndex(['alias', 'user_id'], ['unique' => true])
            ->save();
    }
}
