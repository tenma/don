<?php

class MessageHelper extends \Prefab
{
    /**
     * Cette classe sert à afficher le code HTML stocké dans un tableau passé en argument
     * et avec une classe éventuelle ajoutée en CSS
     */
    public static function render($node)
    {
        $attr = $node['@attrib'];
        if (!isset($attr['messages'])) {
            return;
        }

        $html = '';
        $class = isset($attr['class']) ? $attr['class'] : "";
        $messages = \Template::instance()->token($attr['messages']);
        $html .= '<?php \MessageHelper::instance()->build('.$messages.', "'.$class.'"); ?>';

        return sprintf($html);
    }

    public function build($messages, $class)
    {
        $html = '';
        foreach ($messages as $message) {
            if (strlen($message) > 0) {
                $html .= "<div class=\"$class\">";
                $html .= $message;
                $html .= "</div>";
            }
        }
        echo $html;
    }
}
