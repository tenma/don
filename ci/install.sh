#!/bin/bash
# Ce script est utilisé pour paramétrer l'environnement du site
if [ "x$PROD_HOME" != "x" ]
then
    HOME=$PROD_HOME
else
    HOME=/var/www/dons/
fi

if [ -e "$HOME/app/env" ]
then
    source $HOME/app/env
fi    

cd $HOME
git reset --hard
git pull origin $CI_BUILD_REF_NAME
git checkout $CI_BUILD_REF_NAME
git submodule init
git submodule update

composer install --no-dev
composer update

# Let's run the migrations
if [ -e "$HOME/phinx.yml" ]
then
    case $CI_BUILD_REF_NAME in
        master)
            echo "master"
        	php vendor/robmorgan/phinx/bin/phinx migrate -c $HOME/phinx.yml -e production;;
        preprod)
            echo "preprod"
        	php vendor/robmorgan/phinx/bin/phinx migrate -c $HOME/phinx.yml -e development;;
    esac
else
    case $CI_BUILD_REF_NAME in
        master)
            echo "init master"
        	php vendor/robmorgan/phinx/bin/phinx init -e production;;
        preprod)
            echo "init preprod"
        	php vendor/robmorgan/phinx/bin/phinx init -e development;;
    esac
fi
