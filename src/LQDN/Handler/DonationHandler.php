<?php

namespace LQDN\Handler;

use Doctrine\DBAL\Connection;
use LQDN\Command\DonationCreateCommand;
use LQDN\Command\DonationInvalidateCommand;
use LQDN\Command\DonationResetPdfCommand;
use LQDN\Command\DonationValidateCommand;
use LQDN\Command\DonationIncStatusCommand;

class DonationHandler
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Create a donation
     *
     * @param DonationCreateCommand $command
     */
    public function handleDonationCreateCommand(DonationCreateCommand $command)
    {
        $query =<<<EOF
INSERT INTO dons(status, datec, somme, user_id, public, cumul)
VALUES(:status, :datec, :somme, :user_id, :public, :cumul)
EOF;
        $stmt = $this->connection->prepare($query);
        $stmt->bindvalue('status', $command->getStatus());
        $stmt->bindvalue('datec', $command->getDateC());
        $stmt->bindvalue('somme', $command->getSomme());
        $stmt->bindvalue('user_id', $command->getUserId());
        $stmt->bindvalue('public', $command->getPublic());
        $stmt->bindvalue('cumul', $command->getCumul());
        $stmt->execute();
    }

    /**
     * Validate a donation.
     *
     * @param DonationValidateCommand $command
     */
    public function handleDonationValidateCommand(DonationValidateCommand $command)
    {
        $this->connection->executeUpdate('UPDATE dons SET status = status + 1 WHERE id = :id AND status IN (0, 100)', ['id' => $command->getId()]);
    }

    /**
     * Invalidate a donation.
     *
     * @param DonationInvalidateCommand $command
     */
    public function handleDonationInvalidateCommand(DonationInvalidateCommand $command)
    {
        $this->connection->executeUpdate('UPDATE dons SET status = status - 1 WHERE id = :id AND status IN (1, 101)', ['id' => $command->getId()]);
    }

    /**
     * Reset the PDF of a donation.
     *
     * @param DonationResetPdfCommand $command
     */
    public function handleDonationResetPdfCommand(DonationResetPdfCommand $command)
    {
        $this->connection->executeUpdate('UPDATE dons SET pdf = "" WHERE id = :id', ['id' => $command->getId()]);
    }

    /**
     * Increase the status of a donation
     *
     * @param DonationIncStatusCommand $command
     */
    public function handleDonationIncStatusCommand(DonationIncStatusCommand $command)
    {
        $this->connection->executeUpdate('UPDATE dons SET status = status + 1 WHERE id = :id', ['id' => $command->getId()]);
    }
}
