<?php

namespace LQDN\Command;

class DonationIncStatusCommand
{
    private $donId;

    public function __construct($donId)
    {
        $this->donId = $donId;
    }

    public function getId()
    {
        return $this->donId;
    }
}
