<?php

namespace LQDN\Command;

class CounterpartDeleteCommand
{
    private $counterpartId;

    public function __construct($counterpartId)
    {
        $this->counterpartId = $counterpartId;
    }

    public function getCounterpartId()
    {
        return $this->counterpartId;
    }
}
