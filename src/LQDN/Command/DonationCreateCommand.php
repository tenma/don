<?php

namespace LQDN\Command;

class DonationCreateCommand
{
    private $status;
    private $datec;
    private $somme;
    private $userId;
    private $public;
    private $cumul;

    public function __construct($userId, $status, $datec, $somme, $public, $cumul)
    {
        $this->status = $status;
        $this->userId = $userId;
        $this->datec = $datec;
        $this->somme = $somme;
        $this->public = $public;
        $this->cumul = $cumul;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getDateC()
    {
        return $this->datec;
    }

    public function getSomme()
    {
        return $this->somme;
    }

    public function getPublic()
    {
        return $this->public;
    }

    public function getCumul()
    {
        return $this->cumul;
    }
}
