<?php

namespace LQDN\Command;

class UserCreateCommand
{
    private $email;
    private $hash;
    private $pseudo;
    private $cumul;
    private $total;

    public function __construct($email, $hash, $pseudo, $cumul, $total)
    {
        $this->hash = $hash;
        $this->email = $email;
        $this->pseudo = $pseudo;
        $this->total = $total;
        $this->cumul = $cumul;
    }

    public function getHash()
    {
        return $this->hash;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPseudo()
    {
        return $this->pseudo;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function getCumul()
    {
        return $this->cumul;
    }
}
