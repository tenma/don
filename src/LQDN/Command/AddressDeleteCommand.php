<?php

namespace LQDN\Command;

class AddressDeleteCommand
{
    private $addressId;

    public function __construct($addressId, $userId)
    {
        $this->addressId = $addressId;
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getAddressId()
    {
        return $this->addressId;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }
}
