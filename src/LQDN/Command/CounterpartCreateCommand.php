<?php

namespace LQDN\Command;

class CounterpartCreateCommand
{
    private $counterpartId;
    private $adresseId;
    private $userId;
    private $quoi;
    private $taille;
    private $status;
    private $date;

    public function __construct($addressId, $userId, $quoi, $taille, $status, $date, $commentaire, $parent)
    {
        $this->userId = $userId;
        $this->addressId = $addressId;
        $this->quoi = $quoi;
        $this->taille = $taille;
        $this->status = $status;
        $this->date = $date;
        $this->commentaire = $commentaire;
        $this->parent = $parent;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getAddressId()
    {
        return $this->addressId;
    }

    public function getQuoi()
    {
        return $this->quoi;
    }

    public function getTaille()
    {
        return $this->taille;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getDateCreation()
    {
        return $this->date;
    }

    public function getCommentaire()
    {
        return $this->commentaire;
    }

    public function getParent()
    {
        return $this->parent;
    }
}
