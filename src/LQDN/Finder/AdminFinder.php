<?php

namespace LQDN\Finder;

use Doctrine\DBAL\Connection;

class AdminFinder
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Return all admins.
     *
     * @return array
     */
    public function findAll()
    {
        return $this->connection->fetchAll('SELECT id, user_id FROM admins');
    }
}
