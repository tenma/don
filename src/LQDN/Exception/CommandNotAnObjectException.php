<?php

namespace LQDN\Exception;

class CommandNotAnObjectException extends \InvalidArgumentException
{
}
