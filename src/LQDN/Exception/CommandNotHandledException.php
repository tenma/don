<?php

namespace LQDN\Exception;

class CommandNotHandledException extends \RuntimeException
{
}
