<?php

namespace LQDN;

use LQDN\Exception\CommandNotAnObjectException;
use LQDN\Exception\CommandNotHandledException;

class CommandHandler
{
    private $handlers;

    public function __construct(array $handlers)
    {
        $this->handlers = $handlers;
    }

    /**
     * @param mixed $command
     */
    public function handle($command)
    {
        $method = $this->getHandleMethod($command);

        foreach ($this->handlers as $handler) {
            if (method_exists($handler, $method)) {
                $handler->$method($command);

                return;
            }
        }

        throw new CommandNotHandledException();
    }

    private function getHandleMethod($command)
    {
        if (! is_object($command)) {
            throw new CommandNotAnObjectException();
        }
        $classParts = explode('\\', get_class($command));

        return 'handle' . end($classParts);
    }
}
