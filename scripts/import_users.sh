#!/bin/bash
# Cette monstruosité utilise un import/export par CSV de psql vers mariadb des utilisqteurs nécessaires. On pipe l'un dans l'autre. Il faut exécuter ce script avec des droits sudo.
PSQL_QUERY="COPY (SELECT DISTINCT ON (auth_user.email) email, auth_user.username pseudo FROM auth_user WHERE email NOT LIKE '' AND is_active = 't') TO STDOUT WITH (FORMAT CSV, HEADER, DELIMITER ',');"
MYSQL_QUERY="LOAD DATA LOCAL INFILE '/dev/stdin' INTO TABLE users FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\\"' ESCAPED BY '\\"' LINES TERMINATED BY '\n' IGNORE 1 LINES (email, pseudo);"
sudo -u postgres psql -d soutien -c "$PSQL_QUERY" | mysql $SQL_DATABASE -u $SQL_USER -p$SQL_PASSWORD --local-infile=1 -e "$MYSQL_QUERY"
